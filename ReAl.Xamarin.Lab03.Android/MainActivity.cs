﻿using Android.App;
using Android.Graphics.Drawables;
using Android.Widget;
using Android.OS;
using Android.Provider;

namespace ReAl.Xamarin.Lab03.Android
{
    [Activity(Label = "ReAl.Xamarin.Lab03.Android", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            // SetContentView (Resource.Layout.Main);

            var Helper = new SharedProject.MiClaseCompartida();
            new AlertDialog.Builder(this)
                .SetMessage(Helper.GetFilePath("demo.dat"))
                .Show();
            Validate();
        }

        private async void Validate()
        {
            var client = new SALLab03.ServiceClient();
            var miCorreo = "xxxxxx@gmail.com";
            var miPass = "PASSWORD";
            var miDeviceId = Settings.Secure.GetString(ContentResolver, Settings.Secure.AndroidId);

            SALLab03.ResultInfo resultado = await client.ValidateAsync(miCorreo, miPass, miDeviceId);

            AlertDialog.Builder miDialogo = new AlertDialog.Builder(this);
            AlertDialog miAlert = miDialogo.Create();
            miAlert.SetTitle("Resultado");
            miAlert.SetIcon(Resource.Drawable.Icon);
            miAlert.SetMessage(resultado.Status + "\n" + resultado.Fullname + "\n" + resultado.Token);
            miAlert.SetButton("OK", (sender, args) => { });
            miAlert.Show();
        }


    }
}

